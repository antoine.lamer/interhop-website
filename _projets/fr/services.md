---
name: Nos Services
subtitle: Les services payants que nous proposons
order: 4
image_path: /images/projets/services.png
ref: services
lang: fr
---

### Création de formulation dédié a la santé
Nous utilisons [Goupile](https://goupile.fr/), un formulaire libre et opensource.

### Création de site Internet
Vous voulez un site internet unique. 
Nous utilisions Jekyll pour créer des blogs, site internet.

Tous les thémes que nous créerons seront modifiables et opensources:
- 100% modifiable
- Design adaptable à la taille des écrans
- Markdown
- Valide HTML5
- CSS adapté (Fichiers Sass)
- Section Blog
- Intégration de formulaires, CMS
- Synchronisation Git
- Système de commentaires [Isso](https://posativ.org/isso/) (similaire à Disqus)

Voir les [thèmes Jekyll](https://jekyllthemes.io/free) gratuit.

### Formulaire de contact

<div class="container">
	<form id="serviceForm" action="" method="post" class="contact-form">
	       <label for="besoins">Vos besoins : </label>
		<input type="text" name="besoin" id="besoin" placeholder="Formulaire pour la santé / site Internet" required />
	
	       <br />

		<label for="email">Adresse E-mail : </label>
		<input type="email" name="email_contact" id="email_contact" placeholder="Adresse email" required />
	
	       <br />
	
		<p class="subtext editable">* Aucune information personnelle ne sera pas cedée à qui que se soit !</p>
	
		<input type="submit" value="Envoyer"/>
	</form>
	<br />
	<br />
	<br />

	<p class="subtext editable">

		On vous recontacte :-) <br />
	</p>
</div>
