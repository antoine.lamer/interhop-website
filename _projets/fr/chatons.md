---
name: Visioconférence, cloud, partage de fichiers ... 
subtitle: Interhop.org est un CHATONS
order: 1
external_name: chatons.org
external_url: https://chatons.org/
image_path: /images/projets/chatons.png
ref: chatons
lang: fr
---

### Titre
[Framasoft.org](https://framasoft.org) définit un [CHATONS](https://chatons.org) comme un Collectif des Hébergeurs Alternatifs, Transparents, Ouverts, Neutres et Solidaires.
> Il rassemble des structures souhaitant éviter la collecte et la centralisation des données personnelles au sein de silos numériques du type de ceux proposés par les GAFAM (Google, Apple, Facebook, Amazon, Microsoft).

### Nous adhérons aux valeurs des CHATONS !
Vous pouvez lire la [charte](https://chatons.org/charte) et le [manifeste](https://chatons.org/manifeste) des CHATONS.

### Les services que nous autohébergeons
[forum.interhop.org](http://forum.interhop.org) : outil de messagerie instantanée d'InterHop

[susana.interhop.org](http://susana.interhop.org) : application web nationale d'interopérabilité sémantique

[my.interhop.org](http://my.interhop.org) : gestion et lecture d'articles avec un contenu épuré

[links.interhop.org](http://links.interhop.org) : mur de liens utiles

[git.interhop.org](http://git.interhop.org) : git d'InterHop

[pad.interhop.org](http://pad.interhop.org) : traitement de texte et de [présentations](http://pad.interhop.org/p/HJN5MhVjH#/) en MarkDown

[calc.interhop.org](http://calc.interhop.org) : feuille de calcul

[draw.interhop.org](http://draw.interhop.org) : application de création de diagrammes

[drop.interhop.org](http://drop.interhop.org) : dépot de fichiers avec url

[paste.interhop.org](http://paste.interhop.org) : gestionnaire d'extraits de texte (ou code source)

#### C'est tout ?
N'hésitez pas à demander des nouveaux services si vous en avez besoin!

### Hébergements de Données de Santé
Actuellement ces services ne sont pas certifiés pour héberger des données de santé.

C'est le cas aussi pour Google Drive, WhatsApp...

Nous travaillons activement pour délivrer ces services dont des condtions de sécurité nécessaire pour les données de santé.
Vivement la rentrée...

<p><a style="text-align: center;" class="button alt" type="blue" href="{{ site.baseurl }}/nous/">Lettre d'Information</a></p>

### Budget
500 euros / mois.
Vous pouvez nous aidez. :-)
<p><a style="text-align: center;" class="button alt" type="blue" href="{{ site.baseurl }}/dons/">Dons !</a></p>
