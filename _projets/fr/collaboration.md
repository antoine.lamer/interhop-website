---
name: Nos Collaborations
subtitle: Travaillons ensemble
order: 3
image_path: /images/projets/collaboration.png
ref: collaboration
lang: fr
---

### [Goupile](https://goupile.fr)
<div class="alert alert-blue" markdown="1">
- **But:** Formulaire de recueil de données
- **Techno:**  postgresql, c++, lit-html
- **Contacts:** contact@goupile.fr
- **Site Internet:** [goupile.fr](https://goupile.fr)
</div>

Goupile est une application de saisie électronique de données gratuite et à code source ouvert qui s'efforce de rendre la création de formulaires et la saisie de données à la fois puissantes et faciles.

Il est actuellement en cours de développement et il n'existe pas encore de version publique.

### [Toobib](https://toobib.org)
<div class="alert alert-blue" markdown="1">
- **But:** Application opensource de prise de rendez-vous en santé (phase de test)
- **Gitlab:** https://framagit.org/toobib/toobib-website
- **Techno:** React, Flask, Postgres
- **Contacts:** 
  - interhop@riseup.net
  - contact@toobib.org
- **Site Internet:** [toobib.org](https://toobib.org) en construction
</div>

Toobib.org est une application web permettant la prise de rendez-vous entre professionnels de santé et soignés.

Elle se veut protectrice des données de personnes puisqu’elle ne stocke qu’un courriel associé à un horaire de prise de rendez-vous.

Les futurs développement de Toobib.org veulent promouvoir la décentralisation et la féderation d’instances. Toobib.org utilise aussi les standards d’interopérabilité en santé comme FHIR.

### Transmed
<div class="alert alert-blue" markdown="1">
- **But:** Application opensource de transmission médicale (phase de test)
- **Gitlab:** https://git.magic-lemp.com/Thomas/coviddata-front-patients.git
- **Techno:** React, Django, Postgres
- **Contact:** 
    - alexandre.martin.ecl@gmail.com
    - adrien.parrot@caramail.fr
</div>
