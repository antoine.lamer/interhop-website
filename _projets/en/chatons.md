---
name: Video conferencing, cloud, file sharing ...
subtitle: Interhop.org is a CHATONS
order: 1
external_name: chatons.org
external_url: https://chatons.org/en
image_path: /images/projets/chatons.png
ref: chatons
lang: en
---

### Title

[Framasoft.org](https://framasoft.org) defines [CHATONS](https://chatons.org) as a Collective of Alternative, Transparent, Open, Neutral and Solidary Hosting Providers.
> It gathers structures wishing to avoid the collection and centralization of personal data within digital silos such as those proposed by GAFAM (Google, Apple, Facebook, Amazon, Microsoft).

### We adhere to the CHATONS mindset!

You can read the [charter](https://chatons.org/en/charter) and the [manifesto](https://chatons.org/en/manifesto) of the CHATONS.

### Available services in self-hosting

[forum.interhop.org](http://forum.interhop.org) : InterHop's instant messaging tool

[susana.interhop.org](http://susana.interhop.org) : International web application for managing terminologies

[my.interhop.org](http://my.interhop.org) : managing and reading articles with refined content

[links.interhop.org](http://links.interhop.org) : wall of useful links

[git.interhop.org](http://git.interhop.org) : InterHop git

[pad.interhop.org](http://pad.interhop.org) : word processing and MarkDown [presentations](http://pad.interhop.org/p/HJN5MhVjH#/)

[calc.interhop.org](http://calc.interhop.org) :  spreadsheet

[draw.interhop.org](http://draw.interhop.org) : diagram creation application

[drop.interhop.org](http://drop.interhop.org) : file repository with url

[paste.interhop.org](http://paste.interhop.org) : text extract manager (or source code)

#### Is that all ?
Feel free to ask for new services if you need them

### Hébergements de Données de Santé
Currently these services are not certified to host health data. 

This is also the case for Google Drive, WhatsApp... 

We are actively working to deliver these services including security requirements for health data. Can't wait for the new school year...

<p><a style="text-align: center;" class="button alt" type="blue" href="{{ site.baseurl }}/en/nous/">Newsletter</a></p>

### Budget
500 euros / month.
You can help us. :-)
<p><a style="text-align: center;" class="button alt" type="blue" href="{{ site.baseurl }}/en/dons/">Donate !</a></p>
