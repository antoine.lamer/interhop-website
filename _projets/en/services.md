---
name: Our services
subtitle: The paid services we offer
order: 4
image_path: /images/projets/services.png
ref: services
lang: en
---

### Creation of formulation dedicated to health
We use [Goupile](https://goupile.fr/), a free and open source form.

### Website creation
You want a unique website.
We used Jekyll to create blogs, websites.

All the themes we will create will be editable and open source:
- 100% editable
- Responsive Design
- Markdown
- Valid HTML5
- Adapted CSS (Sass Source Files)
- Section Blog
- Form integration, CMS
- Git synchronization
- [Isso](https://posativ.org/isso/) comment system (similar to Disqus)

[free Jekyll](https://jekyllthemes.io/free) themes.

### Contact form

<div class="container">
	<form id="serviceForm" action="" method="post" class="contact-form">
	       <label for="besoins_en">Vos besoins : </label>
		<input type="text" name="besoin_en" id="besoin_en" placeholder="Health Form / Website" required />
	
	       <br />

		<label for="email_en">Adresse E-mail : </label>
		<input type="email" name="email_contact_en" id="email_contact_en" placeholder="Email Address" required />
	
	       <br />
	
		<p class="subtext editable">* Aucune information personnelle ne sera pas cedée à qui que se soit !</p>
	
		<input type="submit" value="Envoyer"/>
	</form>
	<br />
	<br />
	<br />

	<p class="subtext editable">

		On vous recontacte :-) <br />
	</p>
</div>
