---
name: Our Collaborations
subtitle: Let's work together
order: 3
image_path: /images/projets/collaboration.png
ref: collaboration
lang: en
---

### [Goupile](https://goupile.fr)
<div class="alert alert-blue" markdown="1">
- **Goal:** Data collection form
- **Techno:**  postgresql, c++, lit-html
- **Contacts:** contact@goupile.fr
- **Weebsite:** [goupile.fr](https://goupile.fr)
</div>

Goupile is a free and open-source electronic data capture application that strives to make form creation and data entry both powerful and easy. 

It is currently in development and there are no public builds yet.

### [Toobib](https://toobib.org)
<div class="alert alert-blue" markdown="1">
- **Goal:** Open source health appointment scheduling application (test phase)
- **Gitlab:** https://framagit.org/toobib/toobib-website
- **Techno:** React, Flask, Postgres
- **Contacts:** 
  - interhop@riseup.net
  - contact@toobib.org
- **Site Internet:** [toobib.org](https://toobib.org) under construction
</div>

Toobib.org is a web application for making appointments between healthcare professionals and patients.

It is designed to protect personal data since it only stores an email associated with an appointment scheduling schedule.

The future development of Toobib.org is intended to promote decentralization and the federation of authorities. Toobib.org also uses health interoperability standards such as FHIR.


### Transmed
<div class="alert alert-blue" markdown="1">
- **Goal:** Open source application for medical transmission (test phase)
- **Gitlab:** https://git.magic-lemp.com/Thomas/coviddata-front-patients.git
- **Techno:** React, Django, Postgres
- **Contact:** 
    - alexandre.martin.ecl@gmail.com
    - adrien.parrot@caramail.fr
</div>
