---
layout: post
title: "French government forces hospitals to give up health data to Microsoft"
redirect_from:
  - /le-gouvernement-contraint-les-hopitaux-a-abandonner-vos-donnees-chez-microsoft_en/
lang: en
ref : gouv_order
---

[Sign this text](https://forms.interhop.org/node/16)

An [order](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000041812986&dateTexte=20200421) published on April 21st forces hospitals to intensify the sending of your data to Microsoft.

Contrary to the opinion of many stakeholders - National Commission on Informatics and Liberty, National Council of Physicians, National Council of Lawyers, hospitals - the French government relies on the American giant Microsoft to store all health data.We call for the creation of an academic, media, legal, associative and political ecosystem to reaffirm autonomy and "commons" values and, to generate a broad debate in society.

<!-- more -->

<div class="alert alert-red" markdown="1">
## Microsoft collects your health data
The French National [HealthDataHub](https://solidarites-sante.gouv.fr/actualites/presse/communiques-de-presse/article/communique-de-presse-agnes-buzyn-health-data-hub-officiellement-cree-lundi-2) (HDH) is currently being deployed. The HDH is a one-stop shop for access to all health data to develop artificial intelligence applied to health.
This data belongs to all French citizens and concerns all computerized systems in hospitals, pharmacies, shared medical records and research data from various registries... The amount of data hosted is set to increase exponetially, particularly with the emergence of genomics, imaging and connected objects. All this data is stored at [Microsoft Azure](https://www.lesechos.fr/idees-debats/cercle/opinion-soignons-nos-donnees-de-sante-1143640), the cloud computing platform of the American giant Microsoft.
By presenting a research project of ["public interest"](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000038821260&categorieLien=id) - a legally vague concept - GAFAM (Google, Apple, Facebook, Amazon and Microsoft), start-ups and [insurers](https://www.mediapart.fr/journal/france/221119/health-data-hub-le-mega-fichier-qui-veut-rentabiliser-nos-donnees-de-sante?onglet=full) will now be able to access health data and the financial power they represent. 

This privatization of health is perceived as dangerous for many actors: 
- LREM (presidential majority) Pierre-Alain Raphan deputy is the first to denounce Microsoft's software infrastructure in [LesEchos](https://www.lesechos.fr/idees-debats/cercle/opinion-soignons-nos-donnees-de-sante-1143640)
- Martin Hirsch, director of the Paris Hospitals, is concerned about the risk of "compromising patient confidence" in [Mediapart](https://www.mediapart.fr/journal/france/221119/health-data-hub-le-mega-fichier-qui-veut-rentabiliser-nos-donnees-de-sante). 
- a [collective](https://pad.interhop.org/s/Sk7K8qpaS#) initiated by health and medical informatics professionals worries in a forum at [World](https://interhop.org/en/donnees-de-sante-au-service-des-patients/)
- the [National Council of Lawyers](https://www.cnb.avocat.fr/sites/default/files/11.cnb-mo2020-01-11_ldh_health_data_hubfinal-p.pdf) warns of "the risks of breaching medical confidentiality and invasion of privacy"
- a [collective of publishing software companies](https://www.santenathon.org/) denounces "the failure to respect the principles of equality and transparency in the choice of Microsoft Azure".
- the [National Council of Physicians](https://www.conseil-national.medecin.fr/sites/default/files/external-package/edition/od6gnt/cnomdata_algorithmes_ia_0.pdf) warns that "data infrastructures, as platforms for data collection and exploitation, constitute a major challenge in scientific, economic and cyber security terms. The location of these infrastructures and platforms, their operation, their purposes and their regulation represent a major sovereignty issue so that, tomorrow, France and Europe will not be vassalized by supranational digital giants".
- recently the [National Commission on Informatics and Liberty](https://www.cnil.fr/sites/default/files/atoms/files/deliberation_du_20_avril_2020_portant_avis_sur_projet_darrete_relatif_a_lorganisation_du_systeme_de_sante.pdf) points out that, because of the "sensitivity of the data in question", the storage of data "must be reserved for entities subject exclusively to the jurisdiction of the European Union".
</div>

<div class="alert alert-green" markdown="1">
## Hospitals have been resisting
The French applaud every evening at 8pm to thank and encourage the hospital staff - but also to reaffirm the values of mutual aid and solidarity.

The support extended by the [private sector and the arrival of many volunteers](https://www.aphp.fr/contenu/ap-hp-deja-plus-de-16-000-volontaires-non-soignants-ont-propose-leur-aide-pendant-la-crise) in hospitals during the coronavirus crisis demonstrates the need for technological and human support to help the public sector. However, the public sector must be the guarantor of our rights to our data. For example, the [Paris Hospitals](https://en.wikipedia.org/wiki/Assistance_Publique_%E2%80%93_H%C3%B4pitaux_de_Paris) refused [Palantir's proposal](https://www.bfmtv.com/tech/donnees-de-sante-l-ap-hp-ecarte-la-proposition-de-palantir-1894772.html) - a company submitted to CloudAct and [working for the NSA, FBI and CIA](https://www.france24.com/fr/20200424-covid-19-les-espions-sortent-de-l-ombre-pour-lutter-contre-la-pand%C3%A9mie?ref=li) - to participate in the development of "digital tools for monitoring the Covid-19 outbreak". 

However, an [order](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=LEGITEXT000041812986&dateTexte=20200421) published on April 21st forces hospitals to intensify the sending of your data to HDH-Microsoft, showing a [new fundamental contradiction](https://lvsl.fr/le-health-data-hub-ou-le-risque-dune-sante-marchandisee/) between the logic of unconditional care specific to the public sector and the Hippocratic oath, and the demands for efficiency and profitability nowadays denounced by medical and hospital staff through their strike movement and their reaction to the Covid-19 crisis".
</div>

<div class="alert alert-red" markdown="1">
## CloudAct or the breach of doctor-patient confidentiality ?
In 2018, the US government passed a law called the [Cloud Act](https://www.vie-publique.fr/sites/default/files/rapport/pdf/194000532.pdf) which allows [the US judiciary](https://experiences.microsoft.fr/business/confiance-numerique-business/faut-il-avoir-peur-du-cloud-act/) to access data stored in third countries. The president of the French National Commission for Information Technology and Liberties stated in September at the [National Assembly](http://videos.assemblee-nationale.fr/video.8070927_5d6f64b41f5fe.commission-speciale-bioethique--auditions-diverses-4-septembre-2019?timecode=15077543) that this text is contrary to the [General Regulation on Data Protection](http://www.privacy-regulation.eu/fr/48.htm) (RGPD) which is supposed to protect European citizens. 

In case of political will or cyber attack, patients are subject to a risk of breach of medical secrecy.
What would be the impact of a massive health data leak? 
</div>

<div class="alert alert-green" markdown="1">
## There are technological alternatives

We are in favour of the supervised use of artificial intelligence tools in health care. However, there are alternatives to the GAFAMs that emphasize respect for privacy and medical secrecy. They guarantee independence in the face of supra-state geo-political and commercial issues as well as collective control of infrastructures.

Hospitals create local health data warehouses to collect and analyse data generated in situ. Thanks to this decentralisation, data exchange between regions and our [European neighbours](https://www.ehden.eu/) is possible while preserving data security. 

Hospitals produce and collect data. By bringing care providers and researchers from all fields (including artificial intelligence) into local contact, their expertise promotes the development of new technologies and strengthens the interconnection between care and research.
</div>

<div class="alert alert-orange" markdown="1">
## Call : Coronavirus crisis will create a societal debate
Health data is both a commodity for patients and the inalienable heritage of the community. It is essential to maintain control over the technologies deployed (transparent algorithms, autonomous infrastructures), and to prevent the privatization of health care.
At a time when the risks of [mass surveillance are increasingly topical](https://www.arte.tv/en/videos/083310-000-A/7-billion-suspects/) and when the government wants to use [personal data to combat coronavirus](https://cnnumerique.fr/files/uploads/2020/2020.04.23_COVID19_CNNUM.pdf), the time has come "[to establish our digital autonomy](https://www.lemonde.fr/idees/article/2020/04/25/tracage-numerique-le-moment-est-venu-d-etablir-notre-souverainete-numerique_6037729_3232.html)". 
We are calling for broad information and citizen mobilization. We want to build an academic, media, legal, associative and political ecosystem around these questions to give rise to a broad debate in society.
</div>

[Sign this text](https://forms.interhop.org/node/16)

