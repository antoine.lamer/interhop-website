{
  title: 'Scratch - Direct',
  start: '2020-04-15T16:00:00',
  end: '2020-04-15T17:00:00',
  url: '{{ site.baseurl }}/formations/twitch-scratch3/'
},
{
  title: 'Scratch - Direct',
  start: '2020-04-22T16:00:00',
  end: '2020-04-22T17:00:00',
  url: '{{ site.baseurl }}/formations/twitch-scratch3/'
},
{
  title: 'Scratch - Direct',
  start: '2020-04-29T16:00:00',
  end: '2020-04-29T17:00:00',
  url: '{{ site.baseurl }}/formations/twitch-scratch3/'
},
{
  title: 'Python - Direct',
  start: '2020-04-14T16:00:00',
  end: '2020-04-14T17:00:00',
  url: '{{ site.baseurl }}/formations/twitch-python3/'
},
{
  title: 'Python - Direct',
  start: '2020-04-17T16:00:00',
  end: '2020-04-17T17:00:00',
  url: '{{ site.baseurl }}/formations/twitch-python3/'
},
{
  title: 'Python - Direct',
  start: '2020-04-21T16:00:00',
  end: '2020-04-21T17:00:00',
  url: '{{ site.baseurl }}/formations/twitch-python3/'
},
{
  title: 'Python - Direct',
  start: '2020-04-24T16:00:00',
  end: '2020-04-24T17:00:00',
  url: '{{ site.baseurl }}/formations/twitch-python3/'
},
{
  title: 'Python - Direct',
  start: '2020-04-28T16:00:00',
  end: '2020-04-28T17:00:00',
  url: '{{ site.baseurl }}/formations/twitch-python3/'
},
{
  title: 'Python - Direct',
  start: '2020-05-01T16:00:00',
  end: '2020-05-01T17:00:00',
  url: '{{ site.baseurl }}/formations/twitch-python3/'
},
{
  title: 'Salon Culture & Jeux Mathématiques',
  start: '2020-05-28',
  end: '2020-05-31',
  url: 'http://salon-math.fr/'
},
