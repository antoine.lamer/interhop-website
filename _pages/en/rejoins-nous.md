---
layout: presentation
title: Join the association
subtitle: You want to participate in the construction of free / open source, decentralized and interoperable medical informatics ?
permalink: en/rejoins-nous/
ref: rejoins-nous
lang: en
---

<div class="container">

	<form id="comeForm" action="" method="post" class="contact-form">
		<div class="halves">
			<div class="form-group">
				<label class="checkbox-container">Are you a health professional ?
				  <input type="checkbox" name="pro-sante" id="pro-sante_en" required>
				  <span class="checkmark"></span>
				</label>
	            	</div>
	
			<div class="form-group">
				<label class="checkbox-container">Are you a computer engineer ?
				  <input type="checkbox" name="inge" id="inge_en" required >
				  <span class="checkmark"></span>
				</label>
	            	</div>
		</div>
	
		<div class="form-group">
			<label class="checkbox-container">Are you a student ?
			  <input type="checkbox" name="student" id="student_en" required >
			  <span class="checkmark"></span>
			</label>
	        </div>
		
	       <label for="email">Email Address : </label>
		<input type="email" name="email_conctact_en" id="email_contact_en" placeholder="Email address" required />
	
	       <br />
	
	
		<p class="subtext editable">* Of course no personal information will be given to anyone !</p>
	
		<input type="submit" value="Send"/>
	</form>
	<br />
	<br />
	<br />

	<p class="subtext editable">
                We'll be in touch :-) <br />
		Of course it's not binding.
	</p>
</div>
