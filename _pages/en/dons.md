---
layout: presentation
title: Donate
ref: dons
lang: en
permalink: en/dons/
---

There will later be a link to [HelloAsso.com](https://www.helloasso.com/), a participatory funding site. 

In the meantime you can help us by bank transfer.

## Bank transfer

Transfers from bank to bank are possible free of charge: check with your bank!

Domiciliation : CREDIT COOPERATIF

### Account identification for use in France

- Banque : 42559
- Guichet : 10000
- Compte : 08024330959
- Clé RIB : 32
- BIC : CCOPFRPPXXX

### Account identification for international use (IBAN)

FR76 4255 9100 0008 0243 3095 932

The money will be used to finance servers certified "Health Data Hosting" or HDS to provide you with an HDS [CHATONS.org](https://chatons.org) :-)
