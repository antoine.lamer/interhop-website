---
layout: presentation
title: Dons
permalink: dons/
ref: dons
lang: fr
---

Il y aura plus tard, un lien vers [HelloAsso.com](https://www.helloasso.com/), un site de financement participatif.

En attendant vous pouvez nous aidez par virement bancaire.

## Virement bancaire

Des virements de banque à banque sont possibles sans frais : se renseigner auprès de sa banque !

Domiciliation : CREDIT COOPERATIF

### Identification du compte pour une utilisation nationale

- Banque : 42559
- Guichet : 10000
- Compte : 08024330959
- Clé RIB : 32
- BIC : CCOPFRPPXXX

### Identification du compte pour une utilisation internationale (IBAN)

FR76 4255 9100 0008 0243 3095 932

L'argent servira d'avoir à financer des serveurs certifié "Hébergeur de Données de Santé" ou HDS pour vous fournir un [CHATONS.org](https://chatons.org) HDS. :-)
