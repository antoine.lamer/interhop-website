---
layout: presentation
title: Vous êtes inscrit. Merci.
subtitle: On vous recontacte bientôt.
permalink: /contact-succes/
ref: contact-succes
lang: fr
---

## Merci

Nous utilisons les [listes de diffusion](https://riseup.net/fr/lists) de [Riseup.net](https://riseup.net/).

[Riseup.net](https://riseup.net/) fournit des outils de communication en ligne pour les personnes et les groupes qui militent en faveur d'un changement social libérateur. Nous sommes un projet pour créer des alternatives démocratiques et pour pratiquer l'auto-détermination en contrôlant nos propres moyens de communication sécurisés. 

Les listes de [Riseup.net](https://riseup.net/) n’utilise que des logiciels libres, notamment le programme de gestion de listes courriel sympa, le serveur web apache, le système d’exploitation Debian GNU/Linux et le langage de programmation Perl.

Voici la liste de diffusion d'interhop : [https://lists.riseup.net/www/info/interhop](https://lists.riseup.net/www/info/interhop)

Vous pouvez vous en nous envoyant un courriel ou [ici](https://lists.riseup.net/www/sigrequest/interhop).
