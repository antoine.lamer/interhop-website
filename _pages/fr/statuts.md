---
layout: presentation
title: Statuts
permalink: statuts/
ref: statuts
lang: fr
---

<p style="text-align:center;">
INTERHOP<br/>
ASSOCIATION REGIE PAR LA LOI DU 1er JUILLET 1901<br/>
SIEGE : 142 rue du Faubourg Saint Martin, 75010 PARIS
</p>


<br/>
<br/>
<br/>

# ARTICLE 1 – DENOMINATION
Il est fondé, entre les adhérents appelés membres fondateurs aux présents statuts, une association régie par la loi du 1er juillet 1901 et le décret du 16 août 1901, ayant pour dénomination : InterHop.


# ARTICLE 2 – OBJET
Cette association a pour objet la promotion de l'interopérabilité des systèmes d'information et du logiciel libre en santé et l'hébergement de données de santé générées par l'exploitation de ces logiciels. Ainsi que l’exercice de toute activité liée directement et/ou indirectement à l’objet ci-dessus.


# ARTICLE 3 - SIEGE
Le siège de l’association est fixé 142 rue du Faubourg Saint Martin, 75010 PARIS. Il pourra être transféré par simple décision du conseil d’administration.

# Article 4 – DUREE 
La durée de l’association est illimitée.

# ARTICLE 5 – COMPOSITION 
L’association se compose de membres, personnes physiques et morales, se répartissant en quatre catégories, à savoir :
1. Membres d’honneur, Ceux-ci sont dispensés de cotisation et cooptés par le conseil d’administration en raison des services qu’ils ont rendus ou sont amenés à rendre à l’association.
2. Membres actifs,  Pour être membre actif, il est nécessaire de présenter sa demande ou il est nécessaire d’être présenté par un (ou plusieurs) membres de l’association et d’être agréé par le bureau qui statue souverainement sur les demandes présentées. Les membres actifs s’engagent à respecter les principes définis dans l’article 2 des présents statuts.
3. Membres bienfaiteurs, Ceux-ci versent annuellement une cotisation de soutien dont le montant minimum est fixé par l’Assemblée Générale.

Les personnes morales sont représentées au sein du Conseil d’Administration de l’association par des représentants permanents, lesquels sont obligatoirement des personnes physiques. La qualité de membre s’acquiert dans les circonstances exposées à l’article 6 des présents statuts.

# ARTICLE 6 – ADMISSION
Pour faire partie de l'association, il faut être agréé par le bureau, qui statue, lors de chacune de ses réunions, sur les demandes d'admission présentées. 

# ARTICLE 7 - RADIATIONS 
La qualité de membre se perd par :
1. La démission ;
1. Le décès ;
1. La radiation prononcée par le conseil d'administration pour motif grave, l'intéressé ayant été invité par lettre recommandée à fournir des explications devant le bureau et/ou par écrit. 

# ARTICLE 8 – AFFILIATION 
L’association peut adhérer à d’autres associations, unions ou regroupements par décision du conseil d’administration. Par ailleurs, d’autres associations, unions ou regroupements pourront adhérer à l’association.

# ARTICLE 9 – RESSOURCES
Les ressources de l’association comprennent les cotisations, les financements participatifs (crowdfunding), les dons manuels et contributions, les subventions autorisées par la Loi ainsi que le produit des rétributions perçues au titre des services rendus aux membres ou à des tiers.

# ARTICLE 10 - ASSEMBLEE GENERALE ORDINAIRE 
L'assemblée générale ordinaire comprend tous les membres actifs de l'association.
Elle se réunit chaque année au mois de juin, ainsi qu’à tout autre moment jugé propice par le conseil d’administration. Quinze jours au moins avant la date fixée, les membres de l'association sont convoqués par les soins du secrétaire par voie postale ou numérique (courriel). L'ordre du jour figure sur les convocations. 
Le président, assisté des membres du conseil, préside l'assemblée et expose la situation de l'association. Le trésorier rend compte de sa gestion et soumet les comptes annuels (bilan, compte de résultat et annexe) à l'approbation de l'assemblée. 
Ne peuvent être abordés que les points régulièrement inscrits à l'ordre du jour. 

Les décisions sont prises à la majorité des voix des membres présents ou représentés. 
Il est procédé, après épuisement de l'ordre du jour, au renouvellement des membres sortants du conseil. 

Toutes les délibérations sont prises à main levée.

Les votes ne sont valides que si la moitié des membres du conseil sont présents.

S’agissant de la tenue des réunions, il est possible d’avoir recours aux moyens de communication à distance (conférence téléphonique ou audiovisuelle).

Les décisions des assemblées générales s’imposent à tous les membres, y compris absents ou représentés.

# ARTICLE 11 - ASSEMBLEE GENERALE EXTRAORDINAIRE
Sur la demande du Conseil d’Administration, le président doit convoquer une assemblée générale extraordinaire, suivant les modalités prévues aux présents statuts et uniquement pour modification des statuts ou la dissolution ou pour des actes portant sur des immeubles. Les modalités de convocation sont les mêmes que pour l’assemblée générale ordinaire. Les délibérations sont prises à la majorité des deux tiers des membres présents. Il est possible d’avoir recours aux moyens de communication à distance (conférence téléphonique ou audiovisuelle) afin de realiser l'assemblée générale extraordinaire.

# ARTICLE 12 - CONSEIL D'ADMINISTRATION
L'association est dirigée par un conseil d’administration comprenant trois à cinq membres, élus pour deux années par l'assemblée générale. Les membres sont rééligibles. 

En cas de vacance, le conseil peut pourvoir lui-même au remplacement sous réserve de ratification par la prochaine Assemblée Générale Ordinaire. Les pouvoirs des membres ainsi élus prennent fin à l'expiration du mandat des membres remplacés. Le conseil d'administration se réunit au moins une fois tous les ans, sur convocation du président, ou à la demande du quart de ses membres. Les convocations peuvent être envoyées par voie postale ou numérique (courriel)

Les décisions sont prises à la majorité des voix. 
Il est possible d’avoir recours aux moyens de communication à distance (conférence téléphonique ou audiovisuelle).

Tout membre du conseil qui, sans excuse, n'aura pas assisté à trois réunions consécutives sera considéré comme démissionnaire. 

# ARTICLE 13 – LE BUREAU
Le conseil d'administration élit parmi les membres de l’association, un bureau composé de :
1) Un président ;
2) Un secrétaire général ;
3) Un trésorier. 

Les fonctions de secrétaire général et de trésorier peuvent être exercées par une seule et même personne physique.

L’association est représentée en justice, tant en demande qu’en défense, par un membre du bureau désigné par lui.

L’assemblée constitutive s’est réunie le 30 juin 2020 à Paris et a élu un conseil d’administration qui s’est immédiatement réuni pour élire, dans les conditions prévues à l’article 14 des présents, les premiers membres du bureau.

# Article 14 - ÉLECTION DU BUREAU

Le conseil d’administration élu par l’assemblée générale ordinaire, clôt la dite assemblée, se réunit et vote pour choisir les nouveaux membres du bureau. Les membres de l’association non élus au conseil d’administration peuvent assister à cette élection sans droit de vote.
L’élection du bureau a lieu à main levée sauf si un des membres du conseil d’administration requiert le vote à bulletin secret.
Il est possible d’avoir recours aux moyens de communication à distance (conférence téléphonique ou audiovisuelle).
Les membres sortants sont rééligibles. Une fois les membres du bureau élus, le Président fixe la date de première réunion du bureau.


# ARTICLE 15 – RÉGLEMENT INTÉRIEUR
Un règlement intérieur peut être établi par le conseil d'administration, qui le fait alors approuver par l'assemblée générale. 
Ce règlement éventuel est destiné à fixer les divers points non prévus par les présents statuts, notamment ceux qui ont trait à l'administration interne de l'association.

# ARTICLE 16 – DISSOLUTION
En cas de dissolution prononcée selon les modalités prévues à l’article 11, un ou plusieurs liquidateurs sont nommés, et l'actif, s'il y a lieu, est dévolu conformément aux décisions de l’assemblée générale extraordinaire qui statue sur la dissolution.


Fait à Paris, le 28 novembre 2019

**Adrien PARROT**, Le Président,

**Antoine LAMER**, Le Trésorier,

**Nicolas PARIS**, Le secrétaire
