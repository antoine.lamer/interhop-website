---
layout: presentation
title: Rejoins l'association
subtitle: Vous voulez participer à la construction d’une informatique médicale libre / opensource, décentralisée et interopérable ?
permalink: rejoins-nous/
ref: rejoins-nous
lang: fr
---

<div class="container">

	<form id="comeForm" action="" method="post" class="contact-form">
		<div class="halves">
			<div class="form-group">
				<label class="checkbox-container">Es-tu professionnel.le de santé ?
				  <input type="checkbox" name="pro-sante" id="pro-sante" required>
				  <span class="checkmark"></span>
				</label>
	            	</div>
	
			<div class="form-group">
				<label class="checkbox-container">Es-tu ingénieur.e en informatique ?
				  <input type="checkbox" name="inge" id="inge" required >
				  <span class="checkmark"></span>
				</label>
	            	</div>
		</div>
	
		<div class="form-group">
			<label class="checkbox-container">Es-tu étudiant.e ?
			  <input type="checkbox" name="inge" id="student" required >
			  <span class="checkmark"></span>
			</label>
	        </div>
		
	       <label for="email">Adresse E-mail : </label>
		<input type="email" name="email_contact" id="email_contact" placeholder="Adresse email" required />
	
	       <br />
	
	
		<p class="subtext editable">* Aucune information personnelle ne sera pas cedée à qui que se soit !</p>
	
		<input type="submit" value="Envoyer"/>
	</form>
	<br />
	<br />
	<br />

	<p class="subtext editable">

		On vous recontacte :-) <br />
		Bien sur ca n'engage en rien.
	</p>
</div>
